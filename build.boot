(set-env!
  :source-paths #{"src/clj" "src/cljs"}
  :resource-paths #{"resources/public"}
  :dependencies '[
                   [org.clojure/clojure       "1.8.0"          :scope "provided"]
                   [org.clojure/clojurescript "1.7.228"        :scope "provided"]
                   [adzerk/boot-beanstalk     "0.7.2"          :scope "test"]
                   [adzerk/boot-cljs          "1.7.228-1"      :scope "test"]
                   [pandeiro/boot-http        "0.7.1-SNAPSHOT" :scope "test"]
                   [adzerk/boot-reload        "0.4.4"          :scope "test"]
                   [adzerk/boot-cljs-repl     "0.3.0"          :scope "test"]
                   [com.cemerick/piggieback   "0.2.1"          :scope "test"]
                   [weasel                    "0.7.0"          :scope "test"]
                   [org.clojure/tools.nrepl   "0.2.12"         :scope "test"]
                   [http-kit                  "2.1.19-SNAPSHOT" :scope "provided"]
                   [prone                     "1.1.1"          :scope "test"]
                   [ring                      "1.4.0"          :scope "provided"]
                   [ring/ring-defaults        "0.1.5"          :scope "provided"]
                   [compojure                 "1.5.0"          :scope "provided"]
                   [mount                     "0.1.10"         :scope "provided"]
                   [com.taoensso/sente        "1.8.1"          :scope "provided"]
                   [differ                    "0.3.1"          :scope "provided"]
                   [reagent                   "0.6.0-alpha"    :scope "provided"]
                   [re-frame                  "0.7.0-alpha-3"  :scope "provided"]
                   ])

(require '[adzerk.boot-cljs       :refer [cljs]]
         '[pandeiro.boot-http     :refer [serve]]
         '[adzerk.boot-reload     :refer [reload]]
         '[adzerk.boot-cljs-repl  :refer [cljs-repl start-repl]]
         '[adzerk.boot-beanstalk :refer [beanstalk dockerrun]])

(task-options!
  beanstalk {:name           "caves-3d"
             :version        "0.1.0-SNAPSHOT"
             :description    "My app"
             :access-key     (System/getenv "AWS_ACCESS_KEY")
             :secret-key     (System/getenv "AWS_SECRET_KEY")
             :beanstalk-envs [{;; name must be unique in AWS account
                               :name "Caves-3d"
                               ;; http://<cname-prefix>.elasticbeanstalk.com
                               :cname-prefix "caves3d.23sxpfkjjz.us-west-2"}]}
  pom {:project 'caves-3d
       :version "0.1.0"}
  aot {:namespace '#{caves-3d.core}}
  jar {:file "caves.jar"
       :main 'caves-3d.core
       :manifest {"Description" "Project Description"
                  "Url" "http://myurl.com"}})

(deftask dev
  "Launch dev environment"
  []
  (comp
    ;;     (serve :handler 'caves-3d.core/app
    ;;            :resource-root "target"
    ;;            :reload true
    ;;            :httpkit true
    ;;            :port 3000)
    (watch)

;;         (cljs-repl)
    (reload)
    (cljs :source-map true)

    (target :dir #{"target"})
    (repl :server true)
    ))

(deftask build-docker
  "Build my application docker zip file."
  []
  (comp (add-repo) (dockerrun) (zip)))

(deftask deploy-docker
  "Deploy application docker zip file to AWS EB environment."
  []
  (task-options!
    beanstalk {:stack-name "64bit Amazon Linux 2015.09 v2.0.8 running Docker 1.9.1"})
  identity)

(deftask prod-cljs
  "Compile production files using advanced mode"
  []
  (comp
    (cljs :optimizations :advanced
          :compiler-options {:externs ["externs/three.ext.js" "externs/OrbitControls.ext.js"]})
    (target :dir #{"target"})))

(deftask build
  "Build project uberjar"
  []
  (comp
    (prod-cljs)
    (aot)
    (pom)
    (uber)
    (jar)
    (target :dir #{"target"})))
