FROM java:8-jre

COPY target/caves.jar /srv/app.jar

EXPOSE 3000

WORKDIR /srv

CMD ["java", "-jar", "app.jar"]