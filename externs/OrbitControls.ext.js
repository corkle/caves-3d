'use strict';

var THREE = {};


/**
 * @constructor
 * @param {THREE.Camera} object
 * @param {THREE.HTMLElement=} opt_domElement
 */
THREE.OrbitControls = function(object, opt_domElement) {};


/**
 * @type {THREE.Camera}
 */
THREE.OrbitControls.prototype.object;


/**
 * @type {THREE.HTMLElement}
 */
THREE.OrbitControls.prototype.domElement;


/**
 * @type {boolean}
 */
THREE.OrbitControls.prototype.enabled;


/**
 * @type {THREE.undefined}
 */
THREE.OrbitControls.prototype.target;


/**
 * @type {THREE.undefined}
 */
THREE.OrbitControls.prototype.center;


/**
 * @type {boolean}
 */
THREE.OrbitControls.prototype.enableZoom;


/**
 * @type {number}
 */
THREE.OrbitControls.prototype.zoomSpeed;


/**
 * @type {number}
 */
THREE.OrbitControls.prototype.minDistance;


/**
 * @type {number}
 */
THREE.OrbitControls.prototype.maxDistance;


/**
 * @type {boolean}
 */
THREE.OrbitControls.prototype.enableRotate;


/**
 * @type {number}
 */
THREE.OrbitControls.prototype.rotateSpeed;


/**
 * @type {boolean}
 */
THREE.OrbitControls.prototype.enablePan;


/**
 * @type {number}
 */
THREE.OrbitControls.prototype.keyPanSpeed;


/**
 * @type {boolean}
 */
THREE.OrbitControls.prototype.autoRotate;


/**
 * @type {number}
 */
THREE.OrbitControls.prototype.autoRotateSpeed;


/**
 * @type {number}
 */
THREE.OrbitControls.prototype.minPolarAngle;


/**
 * @type {number}
 */
THREE.OrbitControls.prototype.maxPolarAngle;


/**
 * @type {number}
 */
THREE.OrbitControls.prototype.minAzimuthAngle;


/**
 * @type {number}
 */
THREE.OrbitControls.prototype.maxAzimuthAngle;


/**
 * @type {boolean}
 */
THREE.OrbitControls.prototype.enableKeys;


/**
 * @type {{LEFT: number, UP: number, RIGHT: number, BOTTOM: number}}
 */
THREE.OrbitControls.prototype.keys;


/**
 * @type {{ORBIT: THREE.MOUSE, ZOOM: THREE.MOUSE, PAN: THREE.MOUSE}}
 */
THREE.OrbitControls.prototype.mouseButtons;


/**
 * @type {boolean}
 */
THREE.OrbitControls.prototype.enableDamping;


/**
 * @type {number}
 */
THREE.OrbitControls.prototype.dampingFactor;


/**
 * @param {number=} opt_angle
 */
THREE.OrbitControls.prototype.rotateLeft = function(opt_angle) {};


/**
 * @param {number=} opt_angle
 */
THREE.OrbitControls.prototype.rotateUp = function(opt_angle) {};


/**
 * @param {number=} opt_distance
 */
THREE.OrbitControls.prototype.panLeft = function(opt_distance) {};


/**
 * @param {number=} opt_distance
 */
THREE.OrbitControls.prototype.panUp = function(opt_distance) {};


/**
 * @param {number} deltaX
 * @param {number} deltaY
 */
THREE.OrbitControls.prototype.pan = function(deltaX, deltaY) {};


/**
 * @param {number} dollyScale
 */
THREE.OrbitControls.prototype.dollyIn = function(dollyScale) {};


/**
 * @param {number} dollyScale
 */
THREE.OrbitControls.prototype.dollyOut = function(dollyScale) {};


/**

*/
THREE.OrbitControls.prototype.update = function() {};


/**

*/
THREE.OrbitControls.prototype.reset = function() {};


/**
 * @return {number}
 */
THREE.OrbitControls.prototype.getPolarAngle = function() {};


/**
 * @return {number}
 */
THREE.OrbitControls.prototype.getAzimuthalAngle = function() {};


/**
 * @param {string} type
 * @param {function(event:*)} listener
 */
THREE.OrbitControls.prototype.addEventListener = function(type, listener) {};


/**
 * @param {string} type
 * @param {function(event:*)} listener
 */
THREE.OrbitControls.prototype.hasEventListener = function(type, listener) {};


/**
 * @param {string} type
 * @param {function(event:*)} listener
 */
THREE.OrbitControls.prototype.removeEventListener = function(type, listener) {};


/**
 * @param {{type: string, target: *}} event
 */
THREE.OrbitControls.prototype.dispatchEvent = function(event) {};