#In Progress

- Change direction of movement keys when camera orientiation changes.

- Highlight object when selected.

#Bugs

- Keyboard movement does not update server position.




#Completed


Load initial scene objects when joining/creating a new game. Wait for game load before creating new webgl objects. Should clear the existing objects if from previous game.

How to manage JS objects in memory after deletion.
- It looks like they are handled by GC when not assigned to a parent or name var.

How to force client to create/remove JS objects when entities are added/removed from game-db?
X Look at diff sent from server which comes as vector of an alteration map and removal map.
X if removal map includes {:world {:entities }}, check k/v pairs where v == 0. Dispatch object removal when pred is true.
X if alteration map includes {:world {:entities }}, check each :entities key if it exists in db. Dispatch object creation when pred is false.

I need to create JS objects for each entitity in game-db. This must be done BEFORE any responsive updaters start watching these entities.

#Fixed Bugs

- Mouseover helper plane does not adjust height if mouseclick event fires first. Object will change height to last used height.
FIX: Raycaster was using stale world matrix which was not recaluclated until next rendered frame. Forced updateMatrixWorld after setting new position for mouseplane.
