(ns caves-3d.core
  (:require [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [ring.middleware.reload :refer [wrap-reload]]
;;             [prone.middleware :refer [wrap-exceptions]]
            [org.httpkit.server :refer [run-server]]
            [mount.core :as mount :refer [defstate]]
            [caves-3d.routes :refer [routes]]
            [caves-3d.websocket :as ws]
            )
  (:gen-class))

(def app
  (let [handler (wrap-defaults #'routes site-defaults)]
    (-> handler
;;         wrap-exceptions
        wrap-reload)))

(defonce server_ (atom nil))

(defn stop-web-server! []
  (when-not (nil? @server_)
    (do
      (println "stopping server" @server_)
      (@server_ :timeout 100)
      (reset! server_ nil))))

(defn start-web-server! []
  (println "Starting Web Server...")
  (stop-web-server!)
  (reset! server_ (run-server #'app {:port 3000 :join? false})))

(defstate web-server
  :start (start-web-server!)
  :stop (stop-web-server!))

(defn start! []
  (mount/start))

(defn -main [& args]
  (start!))

