(ns caves-3d.websocket
  (:require [taoensso.sente :as sente]
            [taoensso.sente.server-adapters.http-kit :refer (sente-web-server-adapter)]
            [differ.core :as differ]
            [mount.core :refer [defstate]]
            [clojure.pprint :refer [pprint]]
            [caves-3d.game.game-db :refer [game-db]]
            [caves-3d.game.core :refer [new-game! move-entity]]))

(let [{:keys [ch-recv send-fn ajax-post-fn ajax-get-or-ws-handshake-fn connected-uids]}
      (sente/make-channel-socket! sente-web-server-adapter {:user-id-fn (fn [ring-req] (:client-id ring-req))})]
  (def ring-ajax-post                ajax-post-fn)
  (def ring-ajax-get-or-ws-handshake ajax-get-or-ws-handshake-fn)
  (def ch-chsk                       ch-recv) ; ChannelSocket's receive channel
  (def chsk-send!                    send-fn) ; ChannelSocket's send API fn
  (def connected-uids                connected-uids) ; Watchable, read-only atom
  )

(def subscriptions (atom {:game-session #{}}))
(def subscribers (atom {}))

;; Broadcast Functions
;; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(defn- broadcast
  "Broadcast data to all connected clients"
  ([data]
   (doseq [uid (:any @connected-uids)]
     (chsk-send! uid data)))
  ;;   Broadcast data to all subscribed clients
  ([sub-id data]
   (doseq [id (sub-id @subscriptions)]
     (chsk-send! (name id) data))))

(defn start-game-broadcaster! []
  (add-watch game-db :watch
             (fn [k reference old-state new-state]
               (broadcast :game-session [:game/diff-state (differ/diff old-state new-state)]))))

(defn stop-game-broadcaster! []
  (remove-watch game-db :watch))

(defstate game-broadcaster
  :start (start-game-broadcaster!)
  :stop (stop-game-broadcaster!))

;; Subscription Functions
;; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(defn- add-sub!
  [sub-id id]
  (swap! subscriptions update-in [sub-id] conj id)
  (swap! subscribers update-in [id] conj sub-id))

(defn- remove-sub!
  [sub-id id]
  (swap! subscriptions update-in [sub-id] disj id)
  (swap! subscribers update-in [id] disj sub-id))

(defn- unsubscribe-all!
  [uid]
  (let [id (keyword uid)]
    (doseq [sub-id (id @subscribers)]
      (remove-sub! sub-id id))
    (swap! subscribers dissoc id)))

(defmulti subscribe-to! (fn [sub-id uid] sub-id))

(defmethod subscribe-to! :game-session
  [sub-id uid]
  (add-sub! sub-id (keyword uid))
  (println (str uid " has joined the game session.")))

(defmulti unsubscribe-from! (fn [sub-id uid] sub-id))

(defmethod unsubscribe-from! :game-session
  [sub-id uid]
  (remove-sub! sub-id (keyword uid))
  (println (str uid " has left the game session.")))

;; Sente Event Handlers
;; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(defmulti event-msg-handler :id)

(defn event-msg-handler*
  "Wraps 'event-msg-handler' with logging, error catching, etc."
  [{:as ev-msg :keys [id ?data event]}]
  (event-msg-handler ev-msg))

(defmethod event-msg-handler :default
  [{:as ev-msg :keys [event id ?data ring-req ?reply-fn send-fn]}]
  (println (format "Unhandled event: %s %s" event ?data)))

(defmethod event-msg-handler :chsk/uidport-open
  [{:as ev-msg :keys [uid]}]
  (swap! subscribers assoc (keyword uid) #{})
  (println (str uid " has connected.")))

(defmethod event-msg-handler :chsk/uidport-close
  [{:as ev-msg :keys [uid]}]
  (unsubscribe-all! uid)
  (println (str uid " has disconnected.")))

;;  Game Events
;; -----------------------------------------------------------

(defmethod event-msg-handler :game/new-game
  [{:as ev-msg :keys [uid]}]
  (println (str uid " is creating a new game..."))
  (new-game!))

(defmethod event-msg-handler :game/join
  [{:as ev-msg :keys [uid]}]
  (subscribe-to! :game-session uid)
  (chsk-send! uid [:game/sync-new-state @game-db])
  )

(defmethod event-msg-handler :game/leave
  [{:as ev-msg :keys [uid]}]
  (unsubscribe-from! :game-session uid))

(defmethod event-msg-handler :game/sync-state [ev-msg]
  (broadcast [:game/sync-state @game-db]))

(defmethod event-msg-handler :game/move-player
  [{:as ev-msg :keys [uid ?data]}]
  (move-entity :player ?data))

(defmethod event-msg-handler :game/move-entity
  [{:as ev-msg :keys [uid ?data]}]
  (let [{:keys [entity pos]} ?data]
    (move-entity entity pos)))

;; Debug Events
;; -----------------------------------------------------------

(defmethod event-msg-handler :chsk/ws-ping [ev-msg]
  )

(defmethod event-msg-handler :chsk/ping-all
  [{:as ev-msg :keys [?data id client-id]}]
  (broadcast [:chsk/print (str ?data)]))

(defmethod event-msg-handler :chsk/ping-game-sub
  [{:as ev-msg :keys [?data id client-id]}]
  (broadcast :game-session [:chsk/print (str ?data)]))

(defmethod event-msg-handler :chsk/ping
  [{:as ev-msg :keys [id ?data]}]
  (let [db @game-db]
    (println "I've been PINGED!")))

(defmethod event-msg-handler :chsk/list-uids
  [{:as ev-msg :keys [event id client-id uid ring-req]}]
  (broadcast [:chsk/print-obj @subscribers]))

(def obj-id (atom 10))
(defn- get-obj-id []
  (let [id @obj-id]
    (swap! obj-id inc)
    id))
(defn- get-last-obj-id []
  (let [id @obj-id]
    (swap! obj-id dec)
    (dec id)))
(defn- get-current-id []
  (dec @obj-id))

(defn- create-obj-map []
  (let [x (get-obj-id)
        y (get-obj-id)
        z (get-obj-id)]
    {(keyword (str "block" x)) {:type :cube :size [2 2 2] :pos [5 1 5] :rot [0 0 0]}
     (keyword (str "block" y)) {:type :cube :size [2 2 2] :pos [5 1 5] :rot [0 0 0]}
     (keyword (str "block" z)) {:type :cube :size [2 2 2] :pos [5 1 5] :rot [0 0 0]}}))

(defmethod event-msg-handler :chsk/create-obj
  [ev-msg]
  (swap! game-db update-in [:world :entities] conj (create-obj-map)))

(defmethod event-msg-handler :chsk/remove-obj
  [ev-msg]
  (swap! game-db update-in [:world :entities] dissoc (keyword (str "block" (get-last-obj-id))))
  )

(defmethod event-msg-handler :chsk/change-obj
  [ev-msg]
  (swap! game-db update-in [:world :entities :block1 :pos] #(mapv inc %)))

;; Sente event router ('event-msg-handler' loop)
;; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(defonce router_ (atom nil))
(defn stop-router! [] (when-let [stop-f @router_] (stop-f)))
(defn start-router! []
  (stop-router!)
  (println "Starting Router...")
  (reset! router_
          (sente/start-server-chsk-router!
            ch-chsk event-msg-handler*)))

(defstate event-router
  :start (start-router!)
  :stop (stop-router!))

