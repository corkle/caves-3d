(ns caves-3d.game.core
  (:require [caves-3d.game.game-db :refer [game-db]]
            [caves-3d.game.coords :refer [offset-position]]))

(defn new-game! []
  (let [entities {:player {:type :cube :size [5 5 5] :pos [0 2.5 0] :rot [0 0 0] :hex 0xE81E8C}
                  :ne-cube {:type :cube :size [2 2 2] :pos [5 1 5] :rot [0 0 0] :hex 0x58A3D9}
                  :sw-cube {:type :cube :size [1 1 1] :pos [-5 0.5 -5] :rot [0 0 0] :hex 0xFA7832}
                  :ambient-light {:type :ambient-light :hex 0x404040 }
                  :point-light {:type :point-light :hex 0xffffff :intensity 1 :distance 200 :decay 1 :pos [30 40 0]}
                  :spotlight-light {:type :spotlight :hex 0xffffff :shadow? true :pos [15 80 150]}}
        terrain {:plane {:type :floor :size [300 300] :shadow? true :img "img/grass-map.jpg"}
                 :world-orb {:type :sphere :size [1500 160 140] :img "img/stars.jpg"}}]
    (swap! game-db assoc-in [:world] {:entities entities
                                      :terrain terrain})
    ))

(defn move-entity
  [entity new-pos]
;;   (let [player (get-in @game-db [:world :entities :player])
;;         new-pos (offset-position (:pos player) dir)]
    (swap! game-db assoc-in [:world :entities entity :pos] new-pos))
