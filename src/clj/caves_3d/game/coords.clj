(ns caves-3d.game.coords)

(def directions
  {:n [0 0 -0.5]
   :s [0 0 0.5]
   :e [0.5 0 0]
   :w [-0.5 0 0]
   :nw [-0.5 0 -0.5]
   :ne [0.5 0 -0.5]
   :sw [-0.5 0 0.5]
   :se [0.5 0 0.5]})

(defn offset-position [[x y z] dir]
  (let [[off-x off-y off-z] (directions dir)]
    [(+ x off-x) (+ y off-y) (+ z off-z)]))
