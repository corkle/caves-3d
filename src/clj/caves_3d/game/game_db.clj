(ns caves-3d.game.game-db
  (:require [mount.core :as mount :refer [defstate]]))

(defn- init-state []
  {:running? false})

(defstate game-db :start (atom (init-state))
                  :stop :stopped)
