(ns caves-3d.routes
  (:require [caves-3d.websocket :as ws]
            [compojure.core :refer [defroutes GET POST]]
            [compojure.route :refer [not-found files resources]]
            [hiccup.core :refer [html]]
            [hiccup.page :refer [include-js include-css]]))

(def home-page
  (html
    [:html
     [:head
      [:meta {:charset "utf-8"}]
      [:meta {:name "viewport"
              :content "width=device-width, initial-scale=1"}]
      [:link {:rel "stylesheet" :href "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.css"}]
      [:link {:rel "stylesheet" :href "https://storage.googleapis.com/code.getmdl.io/1.0.6/material.blue_grey-red.min.css"}]
      [:link {:rel "stylesheet" :href "https://fonts.googleapis.com/css?family=Lato:400,700"}]
      (include-css "main.css")]
     [:body
      [:div#app-container]
      [:div#stats]
      [:div#debug]

      (include-js "js/three.min.js"
                  "js/OrbitControls.js"
                  "js/stats.min.js"
                  "js/main.js")
      [:script "caves_3d.core.init()"]
      ]]))

(defroutes routes
  (GET "/" [] home-page)
  (GET  "/chsk" req (ws/ring-ajax-get-or-ws-handshake req))
  (POST "/chsk" req (ws/ring-ajax-post                req))

  (files "/" {:root "target"})
  (resources "/" {:root "target"})
  (not-found "Page Not Found"))
