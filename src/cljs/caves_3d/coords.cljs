(ns caves-3d.coords)

(def directions
  {:n [0 0 -0.5]
   :s [0 0 0.5]
   :e [0.5 0 0]
   :w [-0.5 0 0]
   :nw [-0.5 0 -0.5]
   :ne [0.5 0 -0.5]
   :sw [-0.5 0 0.5]
   :se [0.5 0 0.5]})

(defn offset-position
  [[x y z] dir]
  (let [[off-x off-y off-z] (directions dir)]
    [(+ x off-x) (+ y off-y) (+ z off-z)]))

;; (defn offset-cam-position [[x y z] [cam-x _ cam-z] dir]
;;   (case dir
;;     :forward [(+ x cam-x) y (+ z cam-z)]
;;     :back [(+ x (- cam-x)) y (+ z (- cam-z))]
;;     :left [(+ x cam-z) y (+ z cam-x)]
;;     :right [(+ x (- cam-z)) y (+ z (- cam-x))]
;;     ))

(defn Vector3->pos
  [v]
  (let [x (aget v "x")
        y (aget v "y")
        z (aget v "z")]
    [x y z]))

(defn pos->Vector3
  [[x y z]]
  (THREE.Vector3. x y z))

(defn degrees->radians
  [deg]
  (* deg (/ (.-PI js/Math) 180)))

(defn normalize-pos
  [[x1 y1 z1] [x2 y2 z2]]
  [(- x1 x2) (- y1 y2) (- z1 z2)])

(defn denormalize-pos
  [[x1 y1 z1] [x2 y2 z2]]
  [(+ x1 x2) (+ y1 y2) (+ z1 z2)])

(defmulti apply-rotation-matrix
  (fn [pos dir theta] dir))

(defmethod apply-rotation-matrix :cw
  [[x y z] dir theta]
  [(+ (* x (.cos js/Math theta)) (* (- z) (.sin js/Math theta)))
   y
   (+ (* x (.sin js/Math theta)) (* z (.cos js/Math theta)))])

(defmethod apply-rotation-matrix :ccw
  [[x y z] dir theta]
  [(+ (* x (.cos js/Math theta)) (* z (.sin js/Math theta)))
   y
   (+ (* (- x) (.sin js/Math theta)) (* z (.cos js/Math theta)))])
