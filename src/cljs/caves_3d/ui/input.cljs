(ns caves-3d.ui.input
  (:require [re-frame.core :refer [dispatch register-handler subscribe]]
            [caves-3d.coords :refer [Vector3->pos]]))

;; KEYBOARD INPUT
;; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(def key-name
  {65 :a
   87 :w
   68 :d
   83 :s
   81 :q
   69 :e
   90 :z
   88 :x
   80 :p
   13 :enter
   27 :escape})

;; Process Keyboard Input
;; -----------------------------------------------------------

(defn- update-input
  [game inputs]
  (cond
    (contains? inputs :escape) (dispatch [:input/clear-selection])
    (and (contains? inputs :w) (contains? inputs :a)) (dispatch [:move-selection :nw])
    (and (contains? inputs :w) (contains? inputs :d)) (dispatch [:move-selection :ne])
    (and (contains? inputs :s) (contains? inputs :a)) (dispatch [:move-selection :sw])
    (and (contains? inputs :s) (contains? inputs :d)) (dispatch [:move-selection :se])
    (contains? inputs :w) (dispatch [:move-selection :n])
    (contains? inputs :s) (dispatch [:move-selection :s])
    (contains? inputs :a) (dispatch [:move-selection :w])
    (contains? inputs :d) (dispatch [:move-selection :e]))

  (cond
    (contains? inputs :q) (dispatch [:webgl/rotate-camera :cw])
    (contains? inputs :e) (dispatch [:webgl/rotate-camera :ccw])))

(register-handler
  :input/clear-selection
  (fn [db _]
    (assoc-in db [:ui :selection] nil)))

(register-handler
  :input/update
  (fn [db _]
    (let [keysdown (get-in db [:ui :input :keysdown])]
      (if (> (count keysdown) 0)
        (let [game (:game db)]
          (update-input game keysdown)))
      db)))

(defmulti process-input
  (fn [screen game inputs]
    screen))

(defmethod process-input :start
  [screen game inputs]
  (cond
    (contains? inputs :enter) (dispatch [:game/join])
    (contains? inputs :escape) (dispatch [:go-to-screen :lose])
    :else nil))

(defmethod process-input :win
  [screen game inputs]
  (cond
    (contains? inputs :enter) (dispatch [:go-to-screen :start])
    :else nil))

(defmethod process-input :lose
  [screen game inputs]
  (cond
    (contains? inputs :enter) (dispatch [:go-to-screen :start])
    :else nil))

(defmethod process-input :play
  [screen game inputs]
  (cond
    (contains? inputs :enter) (dispatch [:game/leave]))
  ;;     (contains? inputs :escape) (do
  ;;                                  (dispatch [:go-to-screen :lose])
  ;;                                  (dispatch [:end-game]))
  )

(register-handler
  :input/keydown
  (fn [db [_ key-pressed]]
    (let [screen (get-in db [:ui :current-screen])
          game (:game db)
          keysdown (get-in db [:ui :input :keysdown])
          keysdown (conj keysdown key-pressed)]
      (process-input screen game keysdown)
      (assoc-in db [:ui :input :keysdown] keysdown))
    ))

(register-handler
  :input/keyup
  (fn [db [_ key-released]]
    (let [keysdown (get-in db [:ui :input :keysdown])
          keysdown (disj keysdown key-released)]
      (assoc-in db [:ui :input :keysdown] keysdown))))


;; MOUSE INPUT
;; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(register-handler
  :input/mouse-selected
  (fn [db [_ entity-name]]
    (-> db
        (assoc-in [:ui :input :mouse-drag] (keyword entity-name))
        (assoc-in [:ui :selection] (keyword entity-name)))))

(register-handler
  :input/clear-mouse-drag
  (fn [db _]
    (assoc-in db [:ui :input :mouse-drag] nil)))

(defn- get-mouse-coords [clientX clientY]
  (let [windowW (.-innerWidth js/window)
        canvasW 900
        canvasH 600
        canvasX (/ (- windowW canvasW) 2)
        canvasY 30
        mouseX (- (* (/ (- clientX canvasX) canvasW) 2) 1)
        mouseY (+ (* (/ (- clientY canvasY) canvasH) -2) 1)]
    [mouseX mouseY]))

(defn- update-mouse-plane!
  "Update position of helper plane if mouse intersects with object"
  []
  (let [intersects (.intersectObjects js/RAYCASTER (.-children js/ENTITIES))]
    (if (> (.-length intersects) 0)
      (do
        (aset js/MOUSEPLANE "position" "y" (aget (.-object (aget intersects 0)) "position" "y"))
        (.updateMatrixWorld js/MOUSEPLANE)))))

(defn- mousedown-game-handler [event]
  (.preventDefault event)
  (let [[mouseX mouseY] (get-mouse-coords (.-clientX event) (.-clientY event))
        mouse-vec (THREE.Vector3. mouseX mouseY 1)]
    (.unproject mouse-vec js/CAMERA)
    (.set js/RAYCASTER (.-position js/CAMERA) (.normalize (.sub mouse-vec (.-position js/CAMERA))))
    (update-mouse-plane!)

    ;; Find all intersected objects
    (let [intersects (.intersectObjects js/RAYCASTER (.-children js/ENTITIES))]
      (if (> (.-length intersects) 0)
        (let [entity-name (.-name (.-object (aget intersects 0)))
              ;;               intersection (.intersectObject js/RAYCASTER js/MOUSEPLANE)
              ]
          ;;           (aset js/CONTROLS "enableRotate" false)
          (dispatch [:input/mouse-selected entity-name])
          (set! js/SELECTION (.-object (aget intersects 0)))

          ;;           ;;           calculate offset
          ;;           (if (> (.-length intersection) 0)
          ;;             (.sub (.copy js/OFFSET (.-point (aget intersection 0))) (.-position js/MOUSEPLANE)))
          ))
      ))
  )

(defn- mousemove-game-handler [event]
  (.preventDefault event)
  (let [[mouseX mouseY] (get-mouse-coords (.-clientX event) (.-clientY event))
        mouse-vec (THREE.Vector3. mouseX mouseY 1)]
    (.unproject mouse-vec js/CAMERA)
    (.set js/RAYCASTER (.-position js/CAMERA) (.normalize (.sub mouse-vec (.-position js/CAMERA))))

    (if js/SELECTION
      ;;       Find position of intersection and reposition object
      (if-let [intersection (aget (.intersectObject js/RAYCASTER js/MOUSEPLANE) 0)]
        (let [new-pos (Vector3->pos (aget intersection "point"))]
          (dispatch [:game/update-drag-pos new-pos])
          ;;             (.copy (.-position js/SELECTION) (.sub (aget intersection "point") js/OFFSET))
          ))
      (update-mouse-plane!)))
  )

(defn- mouseup-game-handler [event]
  (.preventDefault event)
  ;;   (aset js/CONTROLS "enableRotate" true)
  (dispatch [:input/clear-mouse-drag])
  (set! js/SELECTION nil)
  )

(defn- mousedown-handler [event]
  (let [rendering? (subscribe [:webgl/rendering?])]
    (if @rendering?
      (mousedown-game-handler event))))

(defn- mousemove-handler [event]
  (let [rendering? (subscribe [:webgl/rendering?])]
    (if @rendering?
      (mousemove-game-handler event))))

(defn- mouseup-handler [event]
  (let [rendering? (subscribe [:webgl/rendering?])]
    (if @rendering?
      (mouseup-game-handler event))))


;; EVENT HANDLERS
;; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(defn- keydown-handler [event]
  (let [keycode (.-keyCode event)
        key-pressed (key-name keycode)]
    (dispatch [:input/keydown key-pressed])
    ))

(defn- keyup-handler [event]
  (let [keycode (.-keyCode event)
        key-released (key-name keycode)]
    ;;     (js/console.log (str "Keyup: " keycode))
    (dispatch [:input/keyup key-released])
    ))

(defn- window-resize-handler []
  (let [width (.-innerWidth js/window)
        height (.-innerHeight js/window)]
    ;;     (js/console.log "Window resized to: " width height)
    ))

(defn add-event-listeners []
  (.addEventListener js/window "resize" window-resize-handler)
  (.addEventListener js/window "mousedown" mousedown-handler)
  (.addEventListener js/window "mousemove" mousemove-handler)
  (.addEventListener js/window "mouseup" mouseup-handler)
  (.addEventListener js/window "keydown" keydown-handler)
  (.addEventListener js/window "keyup" keyup-handler))
