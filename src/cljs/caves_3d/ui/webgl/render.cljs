(ns caves-3d.ui.webgl.render
  (:require [re-frame.core :refer [subscribe dispatch]]))

(defn render-webgl []
  (let [running? (subscribe [:game-running?])
        entities js/ENTITIES]
    (js/requestAnimationFrame render-webgl)

    (if @running?
      (do
;;         (let [world (.getObjectByName js/WORLD "world")]
;;           (aset world "rotation" "x" (+ (aget world "rotation" "x") .0001))
;;           (aset world "rotation" "y" (+ (aget world "rotation" "y") .0002))
;;           (aset world "rotation" "z" (+ (aget world "rotation" "z") .0001)))

        (.render js/RENDERER js/SCENE js/CAMERA)
        (.update js/STATS)
        (dispatch [:input/update])
;;         (dispatch [:webgl/update-camera])
;;         (js/console.log (.-position js/CAMERA))
        nil))))
