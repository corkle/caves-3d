(ns caves-3d.ui.webgl.core
  (:require [re-frame.core :refer [subscribe dispatch]])
  (:require-macros [reagent.ratom :refer [reaction]]))

(defn- watch-obj
  "Updates position and rotation values of watched JS object."
  [id entity]
  (if-let [obj (.getObjectByName js/ENTITIES (name id))]
    (fn [id entity]
      (let [[pos-x pos-y pos-z] (:pos entity)
            [rot-x rot-y rot-z] (:rot entity)]
        (aset obj "position" "x" pos-x)
        (aset obj "position" "y" pos-y)
        (aset obj "position" "z" pos-z)
        (aset obj "rotation" "x" rot-x)
        (aset obj "rotation" "y" rot-y)
        (aset obj "rotation" "z" rot-z)
        nil))))

(defn- webgl-entities
  "Creates watchers for all entities in :world. Entities must be created as JS objects before watchers can be set." []
  (let [world (subscribe [:world])
        entities (reaction (:entities @world))]
    (fn []
      [:div
       (for [[k v] @entities]
         ^{:key k} [watch-obj k v])])))

(defn- webgl-objects []
  [:div
   [webgl-entities]])

(defn- webgl-load-scene
  "Creates new WebGL objects for scene if game is loaded." []
  (let [loaded? (subscribe [:game/loaded?])]
    (if @loaded?
      (dispatch [:webgl/load-scene-objs]))
    [:div
     [webgl-objects]]))

(defn- webgl-render
  "Begins requestAnimationFrame render loop if not started." []
  (let [webgl-rendering? (subscribe [:webgl/rendering?])]
    (if-not @webgl-rendering?
      (do
        (dispatch [:webgl/render])
        [:div "Rendering WebGL..."])
      [webgl-load-scene])))

(defn- webgl-mount
  "Mounts RENDERER and STATS to DOM containers if not mounted." []
  (let [webgl-mounted? (subscribe [:webgl/mounted?])]
    (if-not @webgl-mounted?
      (do
        (dispatch [:webgl/mount])
        [:div "Mounting WebGL Components..."])
      [webgl-render])))

(defn webgl-component
  "Base container for WebGL canvas. :webgl/initialize will instantiate WebGL renderer, camera, scene, controls, and other helper objects." []
  (let [webgl-initialized? (subscribe [:webgl/initialized?])]
    (fn []
      [:div#webgl
       [:div#webgl-container]
       (if-not @webgl-initialized?
         (do
           (dispatch [:webgl/initialize])
           [:div "Initializing WebGL..."])
         [webgl-mount])])))
