(ns caves-3d.ui.webgl.handlers
  (:require [re-frame.core :refer [register-handler dispatch]]
            [caves-3d.ui.webgl.render :refer [render-webgl]]
            [caves-3d.coords :refer [Vector3->pos pos->Vector3 degrees->radians
                                     normalize-pos denormalize-pos apply-rotation-matrix]]))

(defn- new-webgl-scene! []
  (let [entities (js/THREE.Object3D.)
        terrain (js/THREE.Object3D.)]
    (.remove js/SCENE (.getObjectByName js/SCENE "entities"))
    (.remove js/SCENE (.getObjectByName js/SCENE "terrain"))

    (aset entities "name" "entities")
    (aset terrain "name" "terrain")

    (.add js/SCENE entities)
    (.add js/SCENE terrain)

    (set! js/ENTITIES entities)
    (set! js/TERRAIN terrain)))

(register-handler
  :webgl/reset-webgl
  (fn [db _]
    (update-in db [:ui] dissoc :webgl)))

(register-handler
  :webgl/initialize
  (fn [db _]
    (let [renderer (js/THREE.WebGLRenderer. #js {"antialias" true})
          scene (js/THREE.Scene.)
          camera (js/THREE.PerspectiveCamera. 45 (/ 900 600) .1 3000)
          controls (THREE.OrbitControls. camera (.-domElement renderer))
          helpers (js/THREE.Object3D.)
          grid (js/THREE.GridHelper. 500 5)
          camera-helper-plane (js/THREE.Mesh. (js/THREE.PlaneBufferGeometry. 2000 2000 1 1)
                                              (js/THREE.MeshBasicMaterial. #js {"color" 0x0000ff
                                                                                "visible" false}))
          mouse-plane (js/THREE.Mesh. (js/THREE.PlaneBufferGeometry. 495 295 8 8)
                                      (js/THREE.MeshBasicMaterial. #js {"color" 0xffffff
                                                                        "visible" false}))
          stats (js/Stats.)]
      (.setClearColor renderer 0xDCFAE0)
      (.setSize renderer 900 600)
      (aset renderer "shadowMap" "enabled" true)
      (aset renderer "shadowMapSoft" true)

      (.set (aget camera "position") -20 20 -20)
      (.lookAt camera (.-position scene))

      (aset camera-helper-plane  "rotation" "x" (* -0.5 (.-PI js/Math)))
      (.add helpers camera-helper-plane )
      (aset mouse-plane "rotation" "x" (* -0.5 (.-PI js/Math)))
      (.add helpers mouse-plane)
      (.setColors grid 0xff0000 0x000000)
      (.add helpers grid)

      (aset helpers "name" "helpers")
      (.add scene helpers)

      (aset controls "minDistance" 30)
      ;;       (aset controls "maxDistance" 450)
      (aset controls "minPanX" -400)
      (aset controls "maxPanX" 600)
      (aset controls "minPanY" -10)
      (aset controls "maxPanY" 150)
      (aset controls "minPanZ" -250)
      (aset controls "maxPanZ" 350)
      (aset controls "enableRotate" false)
      ;;       (aset controls "maxPolarAngle" (* (/ (.-PI js/Math) 12) 5))

      (set! js/RENDERER renderer)
      (set! js/SCENE scene)
      (set! js/CAMERA camera)
      (set! js/CONTROLS controls)
      (set! js/GRID grid)
      (set! js/HELPERPLANE camera-helper-plane)
      (set! js/MOUSEPLANE mouse-plane)
      (set! js/RAYCASTER (THREE.Raycaster.))
      (set! js/ROTATION-RAYCASTER (THREE.Raycaster.))
      (set! js/SELECTION)
      (set! js/OFFSET (THREE.Vector3.))

      (aset stats "domElement" "style" "position" "absolute")
      (aset stats "domElement" "style" "left" 0)
      (aset stats "domElement" "style" "top" 0)
      (set! js/STATS stats)

      (new-webgl-scene!)
      (assoc-in db [:ui :webgl :initialized?] true)
      )))

(register-handler
  :webgl/mount
  (fn [db _]
    (let [viewport (.getElementById js/document "webgl-container")]
      ;;       Mount Renderer
      (.appendChild viewport (.-domElement js/RENDERER))
      ;;       Mount Stats
      (.appendChild (.getElementById js/document "webgl") (.-domElement js/STATS))
      (assoc-in db [:ui :webgl :mounted?] true))))

(register-handler
  :webgl/render
  (fn [db _]
    (render-webgl)
    (assoc-in db [:ui :webgl :rendering?] true)))

;; Create WebGL Object Factory Functions
;; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(defmulti create-webgl-object
  (fn [id entity] (:type entity)))

(defmethod create-webgl-object :obj
  [id {:keys [type]}]
  (let [obj (js/THREE.Object3D.)]
    (aset obj "name" id)
    obj))

(defmethod create-webgl-object :ambient-light
  [id {:keys [hex]}]
  (let [light (js/THREE.AmbientLight. hex)]
    (aset light "name" (name id))
    light))

(defmethod create-webgl-object :point-light
  [id {:keys [hex intensity distance decay pos]}]
  (let [[x y z] pos
        light (js/THREE.PointLight. hex intensity distance decay)]
    (aset light "name" (name id))
    (.set (.-position light) x y z)
    light))

(defmethod create-webgl-object :spotlight
  [id {:keys [hex pos shadow?]}]
  (let [[x y z] pos
        light (js/THREE.SpotLight. hex)]
    (aset light "name" (name id))
    (.set (.-position light) x y z)
    (aset light "distance" 1000)
    (aset light "castShadow" shadow?)
    (aset light "shadowCameraNear" 5)
    (aset light "shadowCameraFar" 1000)
    (aset light "shadowCameraFov" 100)
    light))

(defmethod create-webgl-object :cube
  [id {:keys [size pos hex]}]
  (let [[w h d] size
        [x y z] pos
        g (js/THREE.BoxGeometry. w h d)
        m (js/THREE.MeshLambertMaterial. #js {"color" hex})
        cube (js/THREE.Mesh. g m)]
    (aset cube "name" (name id))
    (.set (.-position cube) x y z)
    (aset cube "castShadow" true)
    cube))

(defmethod create-webgl-object :sphere
  [id {:keys [size pos img]}]
  (let [[r w h] size
        [x y z] pos
        texture (.load (js/THREE.TextureLoader.) img)
        g (js/THREE.SphereGeometry. r w h)
        m (THREE.MeshBasicMaterial. #js {"map" texture, "side" THREE.DoubleSide.})
        sphere (js/THREE.Mesh. g m)]
    (aset sphere "name" (name id))
    (.set (.-position sphere) x y z)
    (aset sphere "castShadow" true)
    sphere))

(defmethod create-webgl-object :floor
  [id {:keys [size img]}]
  (let [[w h] size
        texture (.load (js/THREE.TextureLoader.) img)
        g (js/THREE.PlaneGeometry. w h 100 100)
        m (js/THREE.MeshLambertMaterial. #js {"map" texture})
        plane (js/THREE.Mesh. g m)]
    (aset plane "name" (name id))
    (aset plane "rotation" "x" (* -0.5 (.-PI js/Math)))
    (aset plane "receiveShadow" true)
    plane))

(defn- create-scene-objects!
  [entities terrain]
  (doseq [[k v] entities]
    (let [obj (create-webgl-object k v)]
      (.add js/ENTITIES obj)))
  (doseq [[k v] terrain]
    (let [obj (create-webgl-object k v)]
      (.add js/TERRAIN obj))))

(register-handler
  :webgl/load-scene-objs
  (fn [db _]
    (let [entities (get-in db [:game :world :entities])
          terrain (get-in db [:game :world :terrain])]
      (create-scene-objects! entities terrain))
    db))

(register-handler
  :webgl/create-webgl-object
  (fn [db [_ container-obj id entity]]
    (let [obj (create-webgl-object id entity)]
      (.add container-obj obj))
    db))

(register-handler
  :webgl/remove-webgl-object
  (fn [db [_ container-obj id]]
    (.remove container-obj (.getObjectByName container-obj (name id)))
    (js/console.log (str "Removing Object " (name id) " from " container-obj))
    db))


;; CAMERA Events
;; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
(defn- get-rotated-camera-pos
  [pos focus-pos dir rotation]
  (-> pos
      (normalize-pos focus-pos)
      (apply-rotation-matrix dir rotation)
      (denormalize-pos focus-pos)))

(defn- get-camera-target! []
  (.set js/ROTATION-RAYCASTER (.-position js/CAMERA) (.normalize (.getWorldDirection js/CAMERA)))
  (let [intersection (aget (.intersectObject js/ROTATION-RAYCASTER js/HELPERPLANE) 0)]
    (aget intersection "point")))

(defn- set-camera-pos!
  [[x y z] target]
  (.set (aget js/CAMERA "position") x y z)
  (.lookAt js/CAMERA target))

(register-handler
  :webgl/animate-camera
  (fn [db [_ dir current-rotation target-rotation target-pos]]
    (if (> current-rotation target-rotation)
      (do
        (set-camera-pos! target-pos (get-camera-target!))
        (aset js/CONTROLS "enabled" true)
        (assoc-in db [:ui :camera :rotating?] false))
      (let [cam-pos (Vector3->pos (aget js/CAMERA "position"))
            cam-target (get-camera-target!)
            rotation (/ target-rotation 60)
            new-pos (get-rotated-camera-pos cam-pos (Vector3->pos cam-target) dir (degrees->radians rotation))]
        (set-camera-pos! new-pos cam-target)
        (js/setTimeout #(dispatch [:webgl/animate-camera dir (+ current-rotation rotation) target-rotation (if (nil? target-pos)
                                                                                                             (get-rotated-camera-pos cam-pos (Vector3->pos cam-target) dir (degrees->radians target-rotation))
                                                                                                             target-pos)]) 16)
        db))))

(register-handler
  :webgl/rotate-camera
  (fn [db [_ dir]]
    (if-not (get-in db [:ui :camera :rotating?])
      (do
        (aset js/CONTROLS "enabled" false)
        (dispatch [:webgl/animate-camera dir 0 90 nil])
        (assoc-in db [:ui :camera :rotating?] true))
      db)))
