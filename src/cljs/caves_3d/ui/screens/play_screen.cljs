(ns caves-3d.ui.screens.play-screen
  (:require [caves-3d.ui.webgl.core :refer [webgl-component]]
            [re-frame.core :refer [dispatch subscribe]]))

(defn- game-play []
  (let []
    (fn []
      [:div
       [webgl-component]

;;        [:div#controls-info
;;         {:style {:padding-top 630
;;                       :margin-left 40}}
;;         [:h4 "Cube Controls"]
;;         [:p "W/S - move along Z plane"]
;;         [:p "A/D - move along X plane"]
;;         [:p "Q/E - rotate on Z plane"]
;;         [:p "Z/X - rotate on Z plane"]
;;         [:p "Left-click cube to drag"]
;;         [:h4 "Camera Controls"]
;;         [:p "Left-click to rotate"]
;;         [:p "Right-click to pan"]
;;         [:p "Scroll wheel/button to zoom"]
;;         [:button.mdl-button.mdl-js-button.mdl-button--raised.mdl-button--colored
;;          {:on-click #(dispatch [:leave-game])}
;;          "Leave Game"]]
       ])))


(defn play-screen []
  (let [game-joined? (subscribe [:game-joined?])]
    (fn []
      [:div#play-screen
       (if @game-joined?
         [game-play]
         [:div "Joining Game..."])]
      )))
