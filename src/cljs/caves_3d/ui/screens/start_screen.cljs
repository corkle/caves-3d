(ns caves-3d.ui.screens.start-screen
  (:require [re-frame.core :refer [subscribe dispatch]]))

(defn start-screen []
  (let []
    (fn []
      [:div#start-screen
       [:div {:style {:margin 100}}
        [:button.mdl-button.mdl-js-button.mdl-button--raised.mdl-button--colored
         {:on-click #(dispatch [:game/join])}
         [:strong "Join Game!"]]]])))
