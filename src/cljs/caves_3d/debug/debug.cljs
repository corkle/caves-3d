(ns caves-3d.debug.debug
  (:require [cljs.pprint :refer [pprint]]
            [re-frame.core :refer [dispatch subscribe register-sub]]
            [reagent.core :as r])
  (:require-macros [reagent.ratom :refer [reaction]]))

(register-sub
  :debug/selection
  (fn [db]
    (reaction (get-in @db [:ui :selection]))))

(register-sub
  :debug/mouse-drag
  (fn [db]
    (reaction (get-in @db [:ui :input :mouse-drag]))))

(declare debug-app-state)

(defn- mount-debug []
  (r/render #'debug-app-state (.getElementById js/document "debug")))

(defn- mount-app-state [get-in-vec]
  (r/render-component [:pre (with-out-str (pprint (get-in @re-frame.db/app-db get-in-vec)))] (.getElementById js/document "app-state")))

(defn- debug-button
  [get-in-vec text]
  [:button {:type "button"
            :on-click #(mount-app-state get-in-vec)}
   text])

(defn- close-debug []
  (r/render-component [:button {:title "Debug app-state"
                                :type "button"
                                :on-click #(mount-debug)
                                :style {:position "fixed"
                                        :bottom 0
                                        :right 0
                                        :margin "5px"}}
                       "+"]
                      (.getElementById js/document "debug")))

(defn- debug-watcher []
  (fn []
    (let [selection (subscribe [:debug/selection])
          drag (subscribe [:debug/mouse-drag])]
      [:div#debug-watcher
       [:p (str "Selection: " @selection)]
       [:p (str "Dragging: " @drag)]])))

(defn debug-app-state []
  [:div {:style {:background-color "#ffcccc"
                 :color "#333333"
                 :z-index 9999}}
   [debug-watcher]

   [:hr]
   [:div#debug-server "debug server events: "
    [:button {:on-click #(dispatch [:ping-server "ping from client!"])} "Ping Server"]
    [:button {:on-click #(dispatch [:ping-game-sub "You are subscribed to :game-session!"])} "Ping :game-session"]
    [:button {:on-click #(dispatch [:game/new-game])} "Create New Game"]
    [:button {:on-click #(dispatch [:game/join])} "Join Game"]
    [:button {:on-click #(dispatch [:game/leave])} "Leave Game"]

    [:button {:on-click #(dispatch [:list-uids])} "List UIDs"]
    ]

   [:hr]
   [:div#debug-game "debug game:"
    [:button {:on-click #(dispatch [:create-object])} "Create Obj"]
    [:button {:on-click #(dispatch [:remove-object])} "Remove Obj"]
    [:button {:on-click #(dispatch [:change-object])} "Change Obj"]
    [:button {:on-click #(dispatch [:go-to-screen :play])} "GoTo Play"]
    [:button {:on-click #(dispatch [:go-to-screen :start])} "GoTo Start"]
    [:button {:on-click #(dispatch [:game/pause])} "Pause Game"]]

   [:hr]
   [:div#debug-webgl "debug webgl:"
    [:button {:on-click #(dispatch [:webgl/reset-scene-objects])} "Reset Scene"]
    [:button {:on-click #(js/console.log js/SCENE)} "SCENE"]
    [:button {:on-click #(js/console.log js/ENTITIES)} "ENTITIES"]
    [:button {:on-click #(js/console.log js/CAMERA)} "CAMERA"]
    [:button {:on-click #(aset js/GRID "visible" (not (aget js/GRID "visible")))} "Toggle Grid"]]

   [:hr]
   [:div [:button {:title "Hide app-state"
                   :type "button"
                   :on-click #(close-debug) :style {:position "fixed"
                                                    :bottom 0
                                                    :right 0
                                                    :margin "5px"}}
          "X"]]
   [:div#app-state-buttons "deref app state: "
    [debug-button [] "App-State"]
    [debug-button [:config] "Config"]
    [debug-button [:ui] "UI"]
    [debug-button [:ui :webgl] "WebGL"]
    [debug-button [:game] "Game"]
    [debug-button [:game :world :entities] "Entities"]]
   [:div#app-state]])

(close-debug)
