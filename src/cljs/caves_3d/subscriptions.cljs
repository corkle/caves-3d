(ns caves-3d.subscriptions
  (:require [re-frame.core :refer [register-sub]])
  (:require-macros [reagent.ratom :refer [reaction]]))

(register-sub
  :db-initialized?
  (fn [db]
    (reaction (not (empty? @db)))))

(register-sub
  :webgl/initialized?
  (fn [db]
    (reaction (get-in @db [:ui :webgl :initialized?]))))

(register-sub
  :webgl/mounted?
  (fn [db]
    (reaction (get-in @db [:ui :webgl :mounted?]))))

(register-sub
  :webgl/rendering?
  (fn [db]
    (reaction (get-in @db [:ui :webgl :rendering?]))))

;; (register-sub
;;   :webgl-ready?
;;   (fn [db]
;;     (get-in @db [:ui :webgl :rendering?])))

(register-sub
  :config
  (fn [db]
    (reaction (:config @db))))

(register-sub
  :current-screen
  (fn [db]
    (reaction (get-in @db [:ui :current-screen]))))

(register-sub
  :game-joined?
  (fn [db]
    (reaction (not (empty? (:game @db))))))

(register-sub
  :game-running?
  (fn [db]
    (reaction (get-in @db [:game :running?]))))

(register-sub
  :game/loaded?
  (fn [db]
    (reaction (get-in @db [:game :loaded?]))))

(register-sub
  :world
  (fn [db]
    (reaction (get-in @db [:game :world]))))
