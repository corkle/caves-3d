(ns caves-3d.handlers
  (:require [re-frame.core :refer [register-handler dispatch]]
            [differ.core :refer [patch]]
            [cljs.pprint :refer [pprint]]
            [caves-3d.ui.webgl.render :refer [render-webgl]]
            [caves-3d.coords :refer [directions Vector3->pos offset-position]]
            [caves-3d.websocket :as ws]))

(enable-console-print!)

(register-handler
  :initialize-db
  (fn [_ _]
    (let [scale-up #(* 10 %)
          world-size [50 30]]
      {
        ;;         :config {:scale-up scale-up
        ;;                 :world-size world-size}
        :ui {:current-screen :start
             :input {:keysdown #{}}
             :webgl {}}})))


;; Navigation
;; ===========================================================

(defn- go-to-screen
  [db screen]
  (-> db
      (assoc-in [:ui :input :keysdown] #{})
      (assoc-in [:ui :current-screen] screen)))

(register-handler
  :go-to-screen
  (fn [db [_ screen]]
    (go-to-screen db screen)))


;; TEMP STUFF ===========================================================



(register-handler
  :update-camera
  (fn [db _]
    (if-let [[x y z] (get-in db [:game :world :entities :player :pos])]
      (let [cam-dir (.getWorldDirection js/CAMERA)
            new-pos (THREE.Vector3. x y z)]
        (aset js/CAMERA "position" "x" (+ x -25))
        (aset js/CAMERA "position" "z" (+ z 60))
        (.lookAt js/CAMERA (THREE.Vector3. x y (+ z 5)))
        ))
    db))

;; (register-handler
;;   :new-game
;;   (fn [db _]
;;     (new-webgl-scene)
;;     (let [config (:config db)
;;           world-size (:world-size config)
;;           scale-up (:scale-up config)
;;           cube (create-cube 5 5 5)
;;           sphere (create-world)
;;           plane (create-tile (mapv scale-up world-size))
;;           spotlight (create-spotlight)
;;           ambient (create-ambient-light 0x999999)]
;;       (-> db
;;           (assoc-in [:game :world :entities] {:cube {:id "cube"
;;                                                      :pos [0 2.5 0]
;;                                                      :rot [0 0 0]}})
;;           (assoc-in [:game :running?] true)
;;           (go-to-screen :play)))))

;; (register-handler
;;   :end-game
;;   (fn [db _]
;;     (-> db
;;         (assoc-in [:game :running?] false)
;;         (assoc-in [:ui :webgl] {:initialized? true}))))

;; (register-handler
;;   :move-cube
;;   (fn [db [_ dir]]
;;     (let [cube-pos (get-in db [:game :world :entities :cube :pos])
;;           [new-x new-y new-z] (offset-position cube-pos dir)
;;           ;;           [new-x new-y new-z] (mapv + (directions dir) cube-pos)
;;           world-size (get-in db [:config :world-size])
;;           scale-up (get-in db [:config :scale-up])
;;           [width height] (mapv scale-up world-size)]
;;       (if (and (<= new-x (/ width 2)) (>= new-x (/ width -2)) (<= new-z (/ height 2)) (>= new-z (/ height -2)))
;;         (assoc-in db [:game :world :entities :cube :pos] [new-x new-y new-z])
;;         db))))

;; (register-handler
;;   :rotate-cube
;;   (fn [db [_ offset]]
;;     (update-in db [:game :world :entities :cube :rot] #(map + offset %))))

;; (register-handler
;;   :update-selection-pos
;;   (fn [db [_ pos]]
;;     (let [selection (keyword (get-in db [:ui :selection]))]
;;       (assoc-in db [:game :world :entities selection :pos] pos))))




;; Websocket Receive Events
;; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(register-handler
  :ws/connected
  (fn [db [_ connected?]]
    (assoc-in db [:ws :connected?] connected?)))

(register-handler
  :game/sync-new-state
  (fn [db [_ new-game]]
    (-> db
        (assoc :game new-game)
        (assoc-in [:game :loaded?] true)
        (assoc-in [:game :running?] true))))

(defn- check-created-objs!
  "If any changed entities/terrain do not already exist, dispatch event to create webgl object."
  [alt-diff world]
  (if-let [alt-ents (get-in alt-diff [:world :entities])]
    (doseq [[k v] alt-ents]
      (if (not (contains? (:entities world) k))
        (dispatch [:webgl/create-webgl-object js/ENTITIES k v]))))
  (if-let [alt-terr (get-in alt-diff [:world :terrain])]
    (doseq [[k v] alt-terr]
      (if (not (contains? (:entities world) k))
        (dispatch [:webgl/create-webgl-object js/TERRAIN k v])))))

(defn- check-removed-objs!
  "If any entities/terrain are marked for removal, dispatch event to remove webgl object."
  [rmv-diff]
  (if-let [rmv-ents (get-in rmv-diff [:world :entities])]
    (doseq [[k v] rmv-ents]
      (if (= v 0)
        (dispatch [:webgl/remove-webgl-object js/ENTITIES k]))))
  (if-let [rmv-terr (get-in rmv-diff [:world :terrain])]
    (doseq [[k v] rmv-terr]
      (if (= v 0)
        (dispatch [:webgl/remove-webgl-object js/TERRAIN k])))))

(register-handler
  :game/diff-state
  (fn [db [_ diff]]
    ;;     (js/console.log (pprint diff))
    (let [alt (first diff)
          rmv (second diff)
          world (get-in db [:game :world])]
      (check-created-objs! alt world)
      (check-removed-objs! rmv))
    (assoc db :game (patch (:game db) diff))))

;; Debug Events
;; -----------------------------------------------------------

(register-handler
  :chsk/print
  (fn [db [_ msg]]
    (js/console.log msg)
    db))

(register-handler
  :chsk/print-obj
  (fn [db [_ obj]]
    (js/console.log (pprint obj))
    db))

(register-handler
  :ping-server
  (fn [db [_ msg]]
    (ws/chsk-send! [:chsk/ping msg])
    db))

(register-handler
  :ping-all-clients
  (fn [db [_ msg]]
    (ws/chsk-send! [:chsk/ping-all msg])
    db))

(register-handler
  :ping-game-sub
  (fn [db [_ msg]]
    (ws/chsk-send! [:chsk/ping-game-sub msg])
    db))

(register-handler
  :list-uids
  (fn [db _]
    (ws/chsk-send! [:chsk/list-uids])
    db))

(register-handler
  :create-object
  (fn [db _]
    (ws/chsk-send! [:chsk/create-obj])
    db))

(register-handler
  :remove-object
  (fn [db _]
    (ws/chsk-send! [:chsk/remove-obj])
    db))

(register-handler
  :change-object
  (fn [db _]
    (ws/chsk-send! [:chsk/change-obj])
    db))

;; GAME
;; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(register-handler
  :game/new-game
  (fn [db _]
    (ws/chsk-send! [:game/new-game])
    db))

(register-handler
  :game/join
  (fn [db _]
    (ws/chsk-send! [:game/join])
    (go-to-screen db :play)))

(register-handler
  :game/leave
  (fn [db _]
    (ws/chsk-send! [:game/leave])
    (dispatch [:webgl/reset-webgl])
    (-> db
        (dissoc db :game)
        (go-to-screen :start))))

(register-handler
  :game/pause
  (fn [db _]
    (update-in db [:game :running?] not)))

;; Player Input Events
;; -----------------------------------------------------------

(register-handler
  :move-player
  (fn [db [_ dir]]
    (let [player (get-in db [:game :world :entities :player])
          new-pos (offset-position (:pos player) dir)]
      (ws/chsk-send! [:game/move-player new-pos])
      (assoc-in db [:game :world :entities :player :pos] new-pos))))

(register-handler
  :move-selection
  (fn [db [_ dir]]
    (if-let [id (get-in db [:ui :selection])]
      (let [selection (get-in db [:game :world :entities id])
            new-pos (offset-position (:pos selection) dir)]
        ;;       (ws/chsk-send! [:game/move-entity {:entity selection :pos new-pos}])
        (assoc-in db [:game :world :entities id :pos] new-pos))
      db)))

(register-handler
  :game/update-drag-pos
  (fn [db [_ new-pos]]
    (let [drag (get-in db [:ui :input :mouse-drag])]
      (ws/chsk-send! [:game/move-entity {:entity drag :pos new-pos}])
      (assoc-in db [:game :world :entities drag :pos] new-pos))))
