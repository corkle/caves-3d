(ns caves-3d.core
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [caves-3d.websocket :refer [start-router!]]
            [caves-3d.handlers]
            [caves-3d.ui.webgl.handlers]
            [caves-3d.subscriptions]
            [caves-3d.debug.debug]
            [caves-3d.ui.input :refer [add-event-listeners]]
            [caves-3d.ui.screens.start-screen :refer [start-screen]]
            [caves-3d.ui.screens.win-screen :refer [win-screen]]
            [caves-3d.ui.screens.lose-screen :refer [lose-screen]]
            [caves-3d.ui.screens.play-screen :refer [play-screen]]))

(defn app-panel []
  (let [screen (rf/subscribe [:current-screen])]
    (fn []
      (case @screen
        :start [start-screen]
        :win [win-screen]
        :lose [lose-screen]
        :play [play-screen]
        [start-screen]))))

(defn loading-panel []
  (let [ready? (rf/subscribe [:db-initialized?])]
    (fn []
      (if-not @ready?
        [:div "Initializing DB..."]
        [app-panel]))))

(defn ^:export init []
  (rf/dispatch [:initialize-db])
  (r/render [loading-panel] (.getElementById js/document "app-container"))
  (start-router!)
  (add-event-listeners))
